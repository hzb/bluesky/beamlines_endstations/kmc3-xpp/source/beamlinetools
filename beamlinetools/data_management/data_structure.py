import time
import os
import socket
from datetime import datetime
import pandas as pd
from event_model import RunRouter


from beamlinetools.callbacks.file_exporter import CSVCallback
from beamlinetools.data_management.specfile_management import spec_factory, new_spec_file


class KMC3XPPDataStructure:
    """
    Docs to be updated
    
    
    This class is used to create the datastructure needed at MySpot

    The class should be initialized in a startup file like this:
    from beamlinetools.data_management.data_structure import MySpotDataStructure    
    mds = MySpotDataStructure()
    
    Then from the ipython session one can create a new experiment
    mds.new_experiment(<'experimental_group'>)
    
    For differen parts of the experiment a new folder with a new specfile
    can be created with:
    mds.newdata(<'experiment_description'>)
        year = str(datetime.now().year)
        commissioning_folder = year + "_commissioning"
        commissioning_abs_path = os.path.join(self._bluesky_data, commissioning_folder)
        if not os.path.exists(commissioning_abs_path):
            os.makedirs(commissioning_abs_path)
        dm = CSVCallback(file_path=self._bluesky_data)
        dm.change_user(commissioning_folder)
        print(f"commissioning folder {commissioning_folder}")
        RE.subscribe(dm.receiver)
    The class takes care also of the persistence of different spec-scan numbers.

    """    

    def __init__(self,RE):
        self.RE = RE
        #self._proposal_number = None # defined by newdata or read from RE.md
        self._set_number =  None# defined by new_experiment, newdata or read from RE.md
        self._bluesky_data = "/opt/bluesky/data"
        self.csv = None
        self._base_name = None
        self._half_of_the_year = None

        # current dateTime
        now = datetime.now()
        # convert to date String
        if now.month < 7:
            self._half_of_the_year = '1'
        elif now.month > 6 and now.month < 13:
            self._half_of_the_year = '2'
        else:
            self._half_of_the_year = None


        self._commissioning_folder = str(datetime.now().year)
        #+ "_commissioning"
        self._commissioning_abs_path = os.path.join(self._bluesky_data, 
                                                    self._commissioning_folder)
        
        self._create_commissioning_folder()
        self._load_existing_keys()
        self._init_and_subscribe_to_csv_export()
        self._set_csv_export_folder()
        self._init_and_subscribe_spec_factory()
        self.print_info()
    
    def print_info(self):

        if self._proposal_number is not None:
            data_folder = os.path.join("/home/bluesky/bluesky/", self._experiment_dir)
            msg = f"The data will be exported in \n{data_folder}"
        else:   
            data_folder = os.path.join("/home/bluesky/bluesky/", self._commissioning_folder)
            msg = f"The data will be exported in \n{data_folder}"
        print()
        print()
        print("##################################################################")
        print(msg)
        print("##################################################################")
        print()
        print()

    def _init_and_subscribe_to_csv_export(self):
        self.csv = CSVCallback(file_path=self._bluesky_data)
        self.RE.subscribe(self.csv.receiver)

    def _set_csv_export_folder(self):
        if self._base_name is None: # if no experiment is set, we use default folder
            self.csv.change_user(self._commissioning_folder)
        else:
            self.csv.change_user(self._experiment_dir)

    def _init_and_subscribe_spec_factory(self):
        if self._base_name is None: # if no experiment is set, we use default folder
            year = time.strftime('%Y')
            spec_factory.file_prefix = 'kmc3-xpp'+year
            spec_factory.directory = self._commissioning_abs_path
        else:
            spec_factory.file_prefix = self._base_name
            spec_factory.directory = self._experiment_dir

        rr = RunRouter([spec_factory])
        self.RE.subscribe(rr)
    
    def commissioning(self):
        # set up csv export folder
        self.csv.change_user(self._commissioning_folder)
        
        # setup specfile
        year = time.strftime('%Y')
        spec_factory.file_prefix = 'kmc3-xpp'+year
        spec_factory.directory = self._commissioning_abs_path
        
    def _create_commissioning_folder(self):
        if not os.path.exists(self._commissioning_abs_path):
            os.makedirs(self._commissioning_abs_path)    
        
    def _load_existing_keys(self):
        # _proposal_number
        try:
            self._proposal_number = self.RE.md["proposal_number"]
            self._experiment_dir = self.RE.md["exported_data_dir"]
        except:
            self._proposal_number = None
        
        # base_name
        try:
            self._base_name = self.RE.md["base_name"]
        except:
            self._base_name = None

        # base_eiger
        try:
            self._base_eiger = self.RE.md["base_eiger"]
        except:
            self._base_eiger = None

        # exported_data_dir
        try:
            self._newdata_dir = self.RE.md['exported_data_dir']
        except:
            self._newdata_dir = None


    def new_experiment(self, proposal_number, base_name):

        self.RE.md["proposal_number"] = proposal_number
        self._proposal_number = proposal_number
        self._experiment_dir = os.path.join(self._bluesky_data, time.strftime('%Y')+'_'+ self._half_of_the_year+'_'+self._proposal_number)
        # Check if the directory existsself._experiment_dir
        if not os.path.exists(self._experiment_dir):
            # If it doesn't exist, create it
            os.makedirs(self._experiment_dir)
        print(f'{self._experiment_dir}')

        if 'scan_id' in self.RE.md.keys():
            print(f'Next scan is number: {self.RE.md["scan_id"]+1}')
        else:
            print(f'Next scan is number: {0}')
        
        # update self variables
        Ymd = time.strftime('%Y-%m-%d')
        self._base_name = Ymd+'_set'+"{:03d}".format(self._set_number)+'_'+base_name   
        self._newdata_dir = os.path.join(self._experiment_dir,self._base_name)


        # Update RE.md with new values
        self.RE.md["set_number"] = self._set_number
        self.RE.md['base_name']  = self._base_name
        self.RE.md['base_eiger'] = os.path.join(self._bluesky_data,
                                           self._experiment_dir,
                                           "eiger")
        self.RE.md['specfile'] = os.path.join(self._experiment_dir,
                                              self._base_name)
        self.RE.md['hostname'] = socket.gethostname() # not sure why/if needed
        self.RE.md['exported_data_dir'] = self._experiment_dir

        # change folder for csv export
        self.csv.change_user(self._experiment_dir) 
        # change specifle name
        spec_factory.file_prefix = self._base_name
        spec_factory.directory = self._experiment_dir
        
