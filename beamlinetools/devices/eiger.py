from ophyd import Component as Cpt
from ophyd import ADComponent as ADCpt
from ophyd import EpicsSignal, EpicsSignalRO,EigerDetector,EigerDetectorCam, TIFFPlugin, TIFFPlugin
from ophyd.areadetector.filestore_mixins import FileStoreTIFFIterativeWrite
from ophyd.areadetector import EigerDetectorCam
from ophyd.areadetector.plugins import HDF5Plugin_V22, PvaPlugin_V31, TIFFPlugin_V22, ROIPlugin_V22, StatsPlugin_V33
from ophyd.areadetector import EpicsSignalWithRBV as SignalWithRBV
from ophyd.status import SubscriptionStatus
import time
from beamlinetools.beamline_config.base import RE
import re
from ophyd.areadetector.filestore_mixins import FileStoreTIFFIterativeWrite, FileStoreTIFF, FileStoreHDF5
class EigerDetectorCamV33(EigerDetectorCam):
    '''This is used to update the Eiger detector to AD33.
    '''
    firmware_version = Cpt(EpicsSignalRO, 'FirmwareVersion_RBV', kind='config')
    ext_trigger_mode = ADCpt(SignalWithRBV, 'ExtGateMode', kind='config')
    wait_for_plugins = Cpt(EpicsSignal, 'WaitForPlugins',
                           string=True, kind='config')
    

    def increment_number_in_string(self, s):
        # Find the last number in the string
        match = re.search(r'\d+$', s)
        if match:
            # Extract the number and increment it
            number = int(match.group())
            incremented_number = number + 1
            # Replace the old number with the incremented number
            new_s = re.sub(r'\d+$', str(incremented_number), s)
            return new_s
        else:
            # Return the original string if no number is found at the end
            return s

    def trigger(self, *args, **kwargs):
        def check_value(*, value, **kwargs):
            "Return True when the acquisition is complete, False otherwise."
            return (value == 0)
            # return (old_value == 0 and value == 1)
        status = SubscriptionStatus(self.acquire, check_value, run=False)

        self.acquire.set(1)
        # time.sleep(1)
        return status

    def stage(self):
        super().stage()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.stage_sigs['wait_for_plugins'] = 'Yes'
        self.stage_sigs['fw_enable'] = 1

    def ensure_nonblocking(self):
        for c in self.parent.component_names:
            cpt = getattr(self.parent, c)
            if cpt is self:
                continue
            if hasattr(cpt, 'ensure_nonblocking'):
                cpt.ensure_nonblocking()


class TIFFPluginWithFileStore(FileStoreTIFF, FileStoreTIFFIterativeWrite):
    ...

class HDF5PluginWithFileStore(FileStoreHDF5, FileStoreTIFFIterativeWrite):  
    ...
    
class EigerDetector(EigerDetector):
    
    new_filepath = RE.md["exported_data_dir"]
    cam = Cpt(EigerDetectorCamV33, 'cam1:') 
    tiff = Cpt(TIFFPluginWithFileStore, 'TIFF1:', write_path_template = f"{new_filepath}")
    hd = Cpt(HDF5PluginWithFileStore, 'HDF1:', write_path_template = f"{new_filepath}")
    pva = Cpt(PvaPlugin_V31, 'Pva1:')
    roi1 = Cpt(ROIPlugin_V22, 'ROI1:')
    roi2 = Cpt(ROIPlugin_V22, 'ROI2:')
    stats1 = Cpt(StatsPlugin_V33, 'Stats1:')
    stats2 = Cpt(StatsPlugin_V33, 'Stats2:')


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cam.wait_for_plugins.set(1)
        self.plugins_list = ['tiff', 'hd', 'pva', 'roi1', 'roi2', 'stats1', 'stats2']

    def stage(self):
        super().stage()

    def trigger(self):
       # set hd5 plugin to capture
       self.hd.capture.set(1)
        
       # set tiff plugin to capture
       self.tiff.capture.set(1)
       
       # Call the cam trigger method 
       return  self.cam.trigger()

    # @property
    # def hints(self):
    #     return {'fields': [self.stats1.total.name]}

    
    def set_acquisition_time(self, time):
        """The aquire time is in seconds"""
        self.cam.acquire_time.set(time)
        self.cam.acquire_period.set(time)

    def set_threshold_energy(self, threshold_energy):
        """ Set threshold energy 1"""
        self.hd.array_callbacks.set(1)
        self.cam.threshold_energy.set(threshold_energy)

    def set_exposures_number(self, exposures_number):
        """ Set number of exposures"""
        self.cam.num_exposures.set(exposures_number)

    def set_hdr_mode(self, verbose=False):
        """ Setup Eiger HDR Trigger Mode"""
        if self.cam.num_triggers.get() != 1:
            self.cam.num_triggers.put(1)
        # set number of exposures per image
        if self.cam.num_exposures.get() != 375000: 
            self.cam.num_exposures.put(375000)
        # set number of images to acquire
        if self.cam.num_images.get() != 1:
            self.cam.num_images.put(1)
        # set trigger mode
        if self.cam.ext_trigger_mode.get() != 0:
            self.cam.ext_trigger_mode.put(0)
        # set trigger mode
        if self.hd.num_capture.get() != 1:
            self.hd.num_capture.put(1)
        # set trigger mode
        if self.tiff.num_capture.get() != 1:
            self.tiff.num_capture.put(1)
        if verbose:
            print(f'self.cam.num_triggers       : {self.cam.num_triggers.get()}')
            print(f'self.cam.trigger_mode       : {self.cam.trigger_mode.get()}')
            print(f'self.cam.num_images         : {self.cam.num_images.get()}')
            print(f'self.cam.ext_trigger_mode   : {self.cam.ext_trigger_mode.get()}')


    def set_pump_and_probe_mode(self, verbose=False):
        """ Setup Eiger Pump and Probe Trigger Mode"""
        if self.cam.num_triggers.get() != 1:
            self.cam.num_triggers.put(1)
        # set number of exposures per image
        if self.cam.num_exposures.get() != 375000: 
            self.cam.num_exposures.put(375000)
        # set number of images to acquire
        if self.cam.num_images.get() != 2:
            self.cam.num_images.put(2)
        # set trigger mode
        if self.cam.ext_trigger_mode.get() != 1:
            self.cam.ext_trigger_mode.put(1)
        # set trigger mode
        if self.hd.num_capture.get() != 2:
            self.hd.num_capture.put(2)
        # set trigger mode
        if self.tiff.num_capture.get() != 2:
            self.tiff.num_capture.put(2)
        if verbose:
            print(f'self.cam.num_triggers        : {self.cam.num_triggers.get()}')
            print(f'self.cam.trigger_mode        : {self.cam.trigger_mode.get()}')
            print(f'self.cam.num_images          : {self.cam.num_images.get()}')
            print(f'self.cam.ext_trigger_mode    : {self.cam.ext_trigger_mode.get()}')

    def enable_plugins(self):
        """ Enable plugins"""
        # Enable Plugins and array callbacks
        for plugin_name in self.plugins_list:
            plugin = getattr(self, plugin_name)
            plugin.enable.set(1)
            plugin.array_callbacks.set(1)

        # Set proper array ports
        self.roi1.nd_array_port.set('EIG')
        self.roi2.nd_array_port.set('EIG')

        # Set propoer array ports for the stats
        self.stats1.nd_array_port.set('ROI1')
        self.stats2.nd_array_port.set('ROI2')



    def prime_plugin(self):
        """ Prime plugins"""
        """ This should be called only once"""
        self.cam.acquire.set(1)

        
    def set_filepath_for_h5_plugin(self, filename='placeholder', filenumber=0):
        """ Settings path for the hd5 plugin"""
        new_filepath = RE.md["base_eiger"]
        self.hd.file_path.set(new_filepath)
        self.hd.file_name.set(filename)
        self.hd.file_number.set(filenumber)
        self.hd.create_directory.set('1')

    def set_filepath_for_tiff_plugin(self, filename='placeholder', filenumber=0):
        """ Settings path for the tiff plugin"""
        new_filepath = RE.md["base_eiger"]
        self.tiff.file_path.set(new_filepath)
        self.tiff.file_name.set(filename)
        self.tiff.file_number.set(filenumber)
        self.tiff.create_directory.set('1')

# number of exposure
# repetition rate (get it from delay generator) * exposure time


# two ROIs: one of the full image, second adjustable
# saved in the SPEC file 
# function to set the ports, names and plugins (at init)
 
# Retries for the IOC EIGER

