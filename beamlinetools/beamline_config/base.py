from bluesky import RunEngine
from os.path import expanduser
from tiled.client import from_uri
from IPython import get_ipython

# Check if we are in ipython shell
is_ipython = get_ipython()

is_ipython.run_line_magic('logstart', '-o -r -t /opt/bluesky/data/log/ipython_logs.log append')

import logging
from logging.handlers import RotatingFileHandler
# Set up logging to capture IPython logs
log_file = '/opt/bluesky/data/log/python_log.log'

# Set maximum size for log file (in bytes)
max_log_size_gb = 1  # 1 gigabyte
max_log_size = max_log_size_gb * 1024 * 1024 * 1024  # Convert gigabytes to bytes

# Create a RotatingFileHandler
log_handler = RotatingFileHandler(log_file, mode='a', maxBytes=max_log_size, backupCount=5)

# Set up formatter
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# Get logger for the root
logger = logging.getLogger()

# Set logging level to capture only messages of INFO level or higher
logger.setLevel(logging.INFO) 

# Add handler to the logger
logger.addHandler(log_handler)

# Add handler to the root logger
logging.root.addHandler(log_handler)


RE = RunEngine({})

from bessyii.callbacks.best_effort import BestEffortCallback
bec = BestEffortCallback()

# Send all metadata/data captured to the BestEffortCallback.
RE.subscribe(bec)

from os import environ
if environ.get('TILED_URL') is not None and environ.get('TILED_API_KEY') is not None:
    if "http" in environ.get('TILED_URL'):
        db = from_uri(environ.get('TILED_URL'), api_key=environ.get('TILED_API_KEY'))

        def post_document(name,doc):
            db.post_document(name,doc)

        RE.subscribe(post_document)

# If we are in ipython shell, we can use the magics
if is_ipython == None:
    #define ip and port for the publisher
    _host = environ.get('ZMQ_URL')

    #create publisher to broadcast documents via 0mq
    publisher = Publisher(_host) 

    #subscribe the publisher to the document stream
    RE.subscribe(publisher)


# Configure persistence between sessions of metadata
# change beamline_name to the name of the beamline
from bluesky.utils import PersistentDict
import os
cwd = os.getcwd()
RE.md = PersistentDict(expanduser('/opt/bluesky/data/persistence/beamline/'))
